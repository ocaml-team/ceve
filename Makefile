include Makefile.config

ifdef USE_OCAMLDUCE
OCAMLFIND = ocamlducefind
OCAML_SOURCES = tools.ml options.ml egraph.ml \
egraph_reader.ml pretty_print.ml \
egraph_writer.ml dose_writer.ml dependencies.ml \
cnf_writer.ml oz_writer.ml graphviz_writer.ml ceve.ml 
else
OCAMLFIND = ocamlfind
OCAML_SOURCES = tools.ml options.ml \
egraph_reader.ml pretty_print.ml \
egraph_writer.ml dose_writer.ml dependencies.ml \
cnf_writer.ml oz_writer.ml graphviz_writer.ml ceve.ml 
endif

OCAML_MLIS = tools.mli options.mli \
egraph_reader.mli pretty_print.mli \
egraph_writer.mli dose_writer.mli dependencies.mli \
cnf_writer.mli graphviz_writer.mli oz_writer.mli
OBJS = $(OCAML_SOURCES:.ml=.cmo)
XBJS = $(OCAML_SOURCES:.ml=.cmx)
CMIS = $(OCAML_MLIS:.mli=.cmi) ceve.cmi

PACKAGES = -package dose2.lifetime,dose2.ocamlpkgsrc,dose2.ocamlrpm,dose2.ocamldeb,dose2.dosebase,str,$(CAMLZIP_NAME)
#ifdef USE_OCAMLDUCE
#PACKAGES+=	,ocamlduce,expat
#DUCEDIR=$(shell ocamlfind query ocamlduce)
#endif

all: ceve
opt: ceve.opt

ceve: $(OBJS)
	$(OCAMLFIND) ocamlc -thread -o $@ -linkpkg $(PACKAGES) $(C_LIBRARIES) $(CCOPTFLAGS) $(OBJS)

ceve.opt: $(XBJS)
# bug in ocamlducefind
ifdef USE_OCAMLDUCE
	ocamlfind ocamlopt -thread -o $@ -linkpkg $(PACKAGES),ocamlduce $(C_LIBRARIES) $(CCOPTFLAGS) $(XBJS)
else
	$(OCAMLFIND) ocamlopt -thread -o $@ -linkpkg $(PACKAGES) $(C_LIBRARIES) $(CCOPTFLAGS) $(XBJS)
endif	

ifdef USE_OCAMLDUCE
egraph_writer.cmo egraph_writer.cmi: ceveduce/egraph_writer.ml egraph_writer.mli
	$(OCAMLFIND) ocamlc -I $(DUCEDIR) $(PACKAGES) -o $@ -c $<

egraph_reader.cmo egraph_reader.cmi: ceveduce/egraph_reader.ml egraph_reader.mli
	$(OCAMLFIND) ocamlc -I $(DUCEDIR) $(PACKAGES) -o $@ -c $<

egraph_writer.cmx: ceveduce/egraph_writer.ml
	$(OCAMLFIND) ocamlopt -I $(DUCEDIR) $(PACKAGES) -o $@ -c $<

egraph_reader.cmx: ceveduce/egraph_reader.ml
	$(OCAMLFIND) ocamlopt -I $(DUCEDIR) $(PACKAGES) -o $@ -c $<
endif

%.cmo: %.ml
	$(OCAMLFIND) ocamlc $(PACKAGES) -c $<

%.cmx: %.ml
	$(OCAMLFIND) ocamlopt $(PACKAGES) -c $<

%.cmi: %.mli
	$(OCAMLFIND) ocamlc $(PACKAGES) -c $<

doc: $(CMIS)
	@rm -rf doc
	@mkdir doc
	$(OCAMLFIND) ocamldoc -html -d doc $(SOURCES)

.PHONY: clean install
clean:
	@rm -rf ceve ceve.opt doc $(OBJS) $(XBJS) $(CMIS) $(OCAML_SOURCES:.ml=.o)

install: ceve
	mkdir -p $(BINDIR)
	if [ -f ceve.opt ] ; then \
	  cp ceve.opt $(BINDIR)/ceve ; \
	else \
	  cp ceve $(BINDIR)/ceve ; \
	fi

depend:
	$(OCAMLFIND) ocamldep $(OCAML_SOURCES) $(OCAML_MLIS) > .depend

include .depend
