(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

(**
Pretty print module
@author Jaap Boender
*)

open Napkin

let out_string = output_string !Options.output_channel

let pretty_print_dep_expression
	(dep_expr: (string, string, string) versioned list list): unit =
begin
  out_string (String.concat ", " (List.map (fun disj -> String.concat " | " (List.map string_of_versioned disj)) dep_expr) ^ "\n")
end;;

let pretty_print_package (pkg: default_package): unit =
begin
	out_string ("Package: " ^ pkg.pk_unit ^ " (" ^ pkg.pk_version ^ ")\n");
	out_string ("Architecture: " ^ pkg.pk_architecture ^ "\n");
	out_string ("Depends: "); pretty_print_dep_expression pkg.pk_depends;
	out_string ("Recommends: "); pretty_print_dep_expression pkg.pk_recommends;
	out_string ("Pre-Depends: "); pretty_print_dep_expression pkg.pk_pre_depends;
	out_string ("Suggests: "); pretty_print_dep_expression pkg.pk_suggests;
	out_string ("Conflicts: " ^ (String.concat ", " (List.map string_of_versioned pkg.pk_conflicts)) ^ "\n");
	out_string ("Replaces: " ^ (String.concat ", " (List.map string_of_versioned pkg.pk_replaces)) ^ "\n");
	out_string ("Provides: " ^ (String.concat ", " (List.map string_of_versioned pkg.pk_provides)) ^ "\n");
	out_string ("Size: " ^ (Int64.to_string pkg.pk_size) ^ "\n");
	out_string ("Installed-Size: " ^ (Int64.to_string pkg.pk_installed_size) ^ "\n");
	(* out_string ("Files: " ^ (String.concat ", " (List.map (fun (f, s) -> f ^ " (" ^ s ^ ")") pkg.files)) ^ "\n"); *)
	flush !Options.output_channel
end;;

let pretty_print (pkgs: default_package list): unit =
begin
	prerr_endline ("[PP] Pretty printing package metadata (" ^ (string_of_int (List.length pkgs)) ^ " packages)...");
	List.iter pretty_print_package pkgs
end;;

let print_sizes (pkgs: default_package list): unit =
begin
	List.iter (fun pkg ->
		out_string (Printf.sprintf "%s'%s@%s %Ld\n" pkg.pk_unit pkg.pk_version pkg.pk_architecture pkg.pk_size)
	) pkgs
end
