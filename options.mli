(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

type input_types = Debian | DebianCache | RPM | HDList | SynHDList | Pkgsrc | EGraphIn
type output_types = PrettyPrint | EGraphOut | (* RPMFind | *) Dose | Oz | Graphviz | TartCNF | TartSize | DimacsCNF
type version_types = Unspecified | DebianVersions | RPMVersions | DeweyVersions
type db_types = MySQL
type db_info_type = {
	db_type: db_types; 
	db_hostname: string;
	db_name: string;
	db_port: int;
	db_user: string;
	db_password: string
}
type dep_types = Conflicts | Depends | Enhances | PreDepends | Recommends | Replaces | Suggests

val output_channel: out_channel ref
val output_dir: string ref
val input_type: input_types ref
val input_files: string list ref
val version_type: version_types ref
val output_type: output_types ref
val resolve_dependencies: bool ref
val database_info: db_info_type ref
val create_tables: bool ref
val verbose: bool ref
val cone_package: (string * string) option ref
val cone_types: dep_types list ref
val cone_stops: (string * string) list ref
val dose_date: (int * int * int) ref
val dose_archive: string ref
val dose_type: string ref
val parse_options: unit 
val ignore_unknown_fields: bool ref
val explicit_conflicts: bool ref
val ignore_file_deps: string ref
