(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

(**
Main module
@author Jaap Boender
*)

open Napkin
open Options

(**
Read the input (whatever type it is) into a list of package metadata
@param files The list of files to be read
@return a list of package metadata
*)
let read_input (files: string list): default_package list =
begin
	match !input_type with
	|	Debian ->
		List.map (fun f -> Napkin.to_default_package (Ocamldeb.read_deb_file f)) files
	| DebianCache ->
		List.flatten (List.map (fun f -> Ocamldeb.read_pool_file f Progress.dummy) files)
	| RPM ->
		List.map Napkin.to_default_package (Ocamlrpm.add_file_provides Progress.dummy
			(List.map (fun f -> Ocamlrpm.read_package "" f) files))
	| HDList ->
		List.map Napkin.to_default_package (Ocamlrpm.add_file_provides Progress.dummy 
			 (List.flatten (List.map (Ocamlrpm.read_hdlist "") files)))
  | SynHDList ->
    List.map Napkin.to_default_package (List.flatten (List.map (Ocamlrpm.read_synthesis_hdlist) files))
	| Pkgsrc ->
		List.flatten (List.map Ocamlpkgsrc.read_summary_file files)
	| EGraphIn -> List.flatten (List.map Egraph_reader.read_egraph_file files)
end;;

(**
Output the metadata in the right format
@param metadata The metadata
*)
let do_output (metadata: default_package list): unit =
begin
	match !output_type with
	| PrettyPrint -> Pretty_print.pretty_print metadata
	| EGraphOut -> Egraph_writer.output_egraph metadata
	(* | RPMFind -> Rpmfind_writer.output_rpmfind metadata *)
	| Dose -> Dose_writer.output_dose metadata
	| Oz -> Oz_writer.output_oz metadata
	| Graphviz -> Graphviz_writer.output_graphviz metadata
	| TartCNF -> Cnf_writer.output_tart_cnf metadata
	| TartSize -> Pretty_print.print_sizes metadata
  | DimacsCNF -> Cnf_writer.output_dimacs_cnf metadata
end;;

(** main function *)
let _ =
let pkgs = ref [] in
begin
	if !verbose then prerr_endline "[Main] Starting ceve.";
	parse_options;
	if !verbose then prerr_endline "[Main] Options parsed.";
	pkgs := read_input (!input_files);
	if !verbose then prerr_endline "[Main] Input read.";
	(* if !version_type = RPMVersions then
		pkgs := Ocamlrpm.add_file_provides !pkgs; *)
	if !version_type = DebianVersions then
		ignore (Ocamldeb.detect_pre_dependency_cycle !pkgs);
	if !resolve_dependencies then
		pkgs := Dependencies.resolve_dependencies !pkgs;
	if !explicit_conflicts then
		pkgs := Dependencies.generate_explicit_conflicts !pkgs;
	begin
		match !cone_package with
		| Some p -> pkgs := (Dependencies.extract_cone !pkgs p)
		| _ -> ()
	end;
	do_output !pkgs;
	if !verbose then prerr_endline "[Main] Output written."
end;;
