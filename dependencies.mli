(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

open Napkin

type pkg_hashtable = (string, string * default_package) Hashtbl.t

val version_of: (string) selector -> string
val compare_versions: string -> string -> int
val can_provide: (string) selector -> (string) selector -> bool
val resolve_dependencies: default_package list -> default_package list

val create_pkgs_hashtable: default_package list -> pkg_hashtable
val extract_cone: default_package list -> string * string -> default_package list
val generate_explicit_conflicts: default_package list -> default_package list
