(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

(**
Datatypes for the EGraph format.
@author Jaap Boender
*)

{{ namespace "http://graphml.graphdrawing.org/xmlns/1.0rc" }}
{{ namespace xlink = "http://www.w3.org/1999/xlink" }}
{{ namespace edos = "http://www.edos-project.org/egraph" }}

type desc_type = {{ <desc>(Latin1) }}

type locator_type = {{ <locator href=`xlink:href type=`xlink:grntfl>[] }}

type node_data_type = {{
	<data key="nodedata" id=?Latin1>[
		<edos:version number=Latin1 filename=?Latin1>[]*
	]
}}

type edge_data_type = {{
	<data key="edgedata" id=?Latin1>[
		<edos:version operator=Latin1 source=Latin1 target=Latin1>[]*
	]
}}

type endpoint_data_type = {{
	<data key="endpointdata" id=?Latin1>[
		<edos:version operator=Latin1 source=Latin1 target=Latin1>[]*
	]
}}

type data_type = {{
	<data key=Latin1>[]
}}

type default_type = {{ <default>(Latin1) }} (* TODO own extensions *)

type port_type = {{
	<port name=Latin1>[
		desc_type?
		(data_type | port_type)*
	]
}}

type key_type = {{
	<key id=Latin1 for=?Latin1>[
		desc_type?
		default_type?
	]
}}

type endpoint_type = {{
	<endpoint id=?Latin1 port=?Latin1 node=Latin1 type=Latin1>[
		desc_type?
		endpoint_data_type?
	]
}}

type graph_type = {{
	<graph id=?Latin1 edgedefault=Latin1 version_type=Latin1>[
		desc_type?
		(
			(data_type | node_type | edge_type | hyperedge_type)* |
			locator_type
		)
	]
}}
and node_type = {{
	<node package=Latin1 id=Latin1>[
		desc_type?
		(
			(
				port_type*
				node_data_type?
				port_type*
				graph_type?
			) |
			locator_type
		)
	]
}}
and edge_type = {{
	<edge id=?Latin1 directed=?Latin1 source=Latin1 target=Latin1 sourceport=?Latin1 targetport=?Latin1 type=Latin1>[
		desc_type?
		edge_data_type?
		graph_type?
	]
}}
and hyperedge_type = {{
	<hyperedge type=Latin1 id=?Latin1>[
		desc_type?
		(data_type | endpoint_type)*
		graph_type?
	]
}}

type graphml_type = {{
	<graphml>[
		desc_type?
		key_type*
		(graph_type|data_type)*
	]
}}
