Ceve - parse package dependencies as set of constraints
-------------------------------------------------------

Ceve is a command line utility used to parse package metadata
information (in particular package interrelationships such as
dependencies) and convert them to set of constraints that need to be
satisfied by a proper package installation.

Supported input formats for package metadata are:
- .deb packages
- Debian package lists (i.e. as in Packages.gz)
- .rpm packages
- RPM package lists
- EGraph (XML based format, derived from GraphML)

Supported output formats for set of constraints are:
- pretty printed format for human consumption
- EGraph
- Dose base, suitable as input for the Pkglab tool
- Oz (a programming language supporting constraint programming)
- Graphviz
- Tart, suitable as input for the Tart media partitioner

