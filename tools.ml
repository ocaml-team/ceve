(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

(**
Module with functions of general interest
@author Jaap Boender
*)

(**
Truncate a string (remove spaces and nulls from either end)
@param s The string to truncate
@return The truncated string
*)
let rec truncate (s: string): string =
begin	
	if s.[0] = ' ' || s.[0] = (Char.chr 0) then
		truncate (String.sub s 1 (String.length s - 1))
	else if s.[String.length s - 1] = ' ' || s.[String.length s - 1] = (Char.chr 0) then
		truncate (String.sub s 0 (String.length s - 1))
	else
		s
end;;

let rec fixpoint (start: 'a) (f: 'a -> 'a): 'a =
let result = f start in
begin
	if result = start then
		result
	else
		fixpoint result f
end;;

let starts_with (needle: string) (haystack: string): bool =
begin
	if String.length haystack < String.length needle then
		false
	else
		String.sub haystack 0 (String.length needle) = needle
end;;

let rec uniq (x: 'a list): 'a list =
begin
	match x with
		[] -> []
	| y::ys ->
			begin
				match ys with
					[] -> [y]
				| z::zs -> if y = z then uniq ys else y::(uniq ys)
			end
end;;
