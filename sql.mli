(*
This file is part of Ceve.

Ceve is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Ceve is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ceve; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*)

open Options
open Mysql

type db_connection = MySQLConnection of dbd
type db_result = MySQLResult of result
type db_statement = MySQLStatement of stmt
type db_parameter =
  DBString of int * string
| DBInt of int
| DBText of string

val open_database: unit -> db_connection
val execute_query: db_connection -> string -> db_result
val prepare_statement: db_connection -> string -> db_statement
val execute_statement: db_statement -> db_parameter array -> unit
val fetch_statement: db_statement -> db_parameter array list
val statement_insert_id: db_statement -> int64
